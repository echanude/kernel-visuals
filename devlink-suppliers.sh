#! /usr/bin/env bash

set -eu
shopt -s nullglob

usage() {
	cat - <<EOF
Usage: $0 SYSFS_DEVICE_PATH
Walk through the suppliers dependencies of a sysfs device and print the resulting tree.
EOF
}

plot_devlink_suppliers() {
	local dev="$1"
	local indent="$2"
	local suppliers driver

	# DFS, left
	if [[ -e "$dev/driver" ]]; then
		driver="$(readlink -e "$dev/driver")"
		printf "%${indent}s$dev [${driver##*/}]\n" ''
	else
		printf "%${indent}s%s\n" '' "$dev"
	fi

	suppliers=("$dev/supplier:"*)
	if ! (( ${#suppliers[@]} )); then
		return 0
	fi

	for sup in "${suppliers[@]}"; do
		if [[ -e "$sup/supplier/device" ]]; then
			plot_devlink_suppliers "$(readlink -e "$sup/supplier/device")" $((indent + 1))
		else
			plot_devlink_suppliers "$(readlink -e "$sup/supplier")" $((indent + 1))
		fi
	done
}

DEV="${1:-}"
if [[ -z "$DEV" ]]; then
	usage
	exit 1
fi
if [[ ! -r "$DEV" ]]; then
	echo "\"$DEV\" does not exist." >&2
	exit 1
fi

plot_devlink_suppliers "$DEV" 0
