#!/usr/bin/env perl
# SPDX-License-Identifier: GPL-2.0-only

use strict;
use warnings;
use bignum (p => -6);
use Data::Dumper;

# %starts = { "<alg_name>" => <time> };
# %ends = { "<alg_name>" => <time> };

# %events = {
# 	"<alg_name>" => (
# 		{
# 			start => <timestamp>;
# 			end => <timestamp>;
# 			rc => <return-code>;
# 		}, {...}
# 	}
# }

my %events;
my %async;

my $first = 999999;
my $last = 0;

sub round($$)
{
	my ($value, $precision) = @_;
	my $factor = 10 ** $precision;
	return int($value * $factor + 0.5) / $factor;
}

while (<>) {
	my $line = $_;
	# Skip comments.
	if ($line =~ /^#.*/) {
		next;
	}
	#                 TASK-PID    CPU#           ||||||    TIMESTAMP    dev_probe_start: dev="<dev>", driver="<driver>"
	if ($line =~ /^\s+([^\s]+)\s+\[([0-9]+)\]\s+([^\s]+)\s+([0-9.]+):\s+dev_probe_start: dev="(.+)", driver="(.+)"/) {
		my $task = $1;
		my $cpu = $2;
		my $flags = $3;
		my $ts = $4;
		my $dev = $5;
		my $drv = $6;
		my $name = "[" . $drv . "] " . $dev;

		# Trust that the data is sane (no two consecutive starts exist)
		push @{ $events{$name} }, { start => $ts, end => 0, rc => -1 };

		if ($first > $ts) {
			$first = $ts;
		}
	}
	#                 TASK-PID    CPU#           ||||||    TIMESTAMP    dev_probe_finish: dev="<dev>",driver="<driver>", rc="<return-code>"
	if ($line =~ /^\s+([^\s]+)\s+\[([0-9]+)\]\s+([^\s]+)\s+([0-9.]+):\s+dev_probe_finish: dev="(.+)", driver="(.+)", rc=(.+)/) {
		my $task = $1;
		my $cpu = $2;
		my $flags = $3;
		my $ts = $4;
		my $dev = $5;
		my $drv = $6;
		my $rc = $7;
		my $name = "[" . $drv . "] " . $dev;

		# Trust that the data is sane (no two consecutive starts exist)
		$events{$name}[-1]{end} = $ts;
		$events{$name}[-1]{rc} = $rc;

		if ($last < $ts) {
			$last = $ts;
		}
	}
	#                 TASK-PID    CPU#           ||||||    TIMESTAMP    dev_probe_async: dev="<dev>", driver="<driver>"
	if ($line =~ /^\s+([^\s]+)\s+\[([0-9]+)\]\s+([^\s]+)\s+([0-9.]+):\s+dev_probe_async: dev="(.+)", driver="(.+)"/) {
		my $task = $1;
		my $cpu = $2;
		my $flags = $3;
		my $ts = $4;
		my $dev = $5;
		my $drv = $6;
		my $name = "[" . $drv . "] " . $dev;

		if (!exists $async{$name}) {
			$async{$name} = 1;
		}
	}
	#print STDERR "$line";
}

#print STDERR Dumper(\%events) . "\n";

my $total_width = 4100;
my $margin = 50;
print "<?xml version=\"1.0\" standalone=\"no\"?>
<svg id=\"chart\" width=\"$total_width\" height=\"100%\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">
<style>
  text { fill:#fc7b7b; font-size:3pt }
</style>

<rect width=\"100%\" height=\"100%\" fill=\"#16161e\"/>
";

my $interline = 5; 	# Space between timelines (used by text).
my $y = $interline;	# Starting offset.
my $h = 30;		# height of the bar.
my $text_offset = 3;	# label horizontal offset next to each bar.
my $unit_offset = 3;	# units vertical offset under each graduation.
my $atom = 2; 		# Dipslay items as block when at least $atom units large.

my @probes = sort { $events{$a}[0]{start} <=> $events{$b}[0]{start} } keys(%events);

my $scale = ($total_width - (2 * $margin)) / ($last - $first); # scale timespan over our unit scale.
my $noise = 0;

print STDERR "$[ $first ; $last ], scale: $scale.\n";

# Display time scale.
my $stride = round(($last - $first) / 100, 3);
my $time = round($first, 3);

print STDERR "[ $first ; $last ], stride: $stride, scale: $scale\n";
while ($time < ($last + $stride / 2)) {
	my $x1 = $margin + ($time - $first) * $scale;
	my $y1 = scalar(@probes) * ($interline + $h) + $h + $unit_offset;
	my $y_unit = scalar(@probes) * ($interline + $h) + $h + $unit_offset;

	print "<text transform=\"translate($x1,$interline)\" text-anchor=\"middle\">$time</text>\n";
	print "<line x1=\"$x1\" y1=\"".($interline+$unit_offset)."\" x2=\"$x1\" y2=\"$y1\" stroke-dasharray=\"1,8\" style=\"stroke:#e0af68;stroke-width:1\"/>\n";
	print "<text transform=\"translate($x1,$y_unit)\" text-anchor=\"middle\">$time</text>\n";
	$time = $time + $stride;
}

# Display time blocks.
my $total_blocks = 0;
foreach my $name (@probes) {
	foreach my $rec (@{ $events{$name} }) {
		my $dt = $rec->{end} - $rec->{start};
		my $x = $margin + ($rec->{start} - $first) * $scale;
		my $w = $dt * $scale;
		my $color = $async{$name} ? "#d9d4cd" : "#2ac3de;";
		my $stroke;

		if ($rec->{rc} == 0) {
			$stroke = ";stroke-width:0.2;stroke:#9ece6a";
		} elsif ($rec->{rc} == -517) {
			$stroke = ";stroke-width:0.2;stroke:#9d7cd8";
		} else {
			$stroke = ";stroke-width:0.2;stroke:#db4b4b";
		}
		print "<rect x=\"$x\" width=\"$w\" y=\"$y\" height=\"$h\" style=\"fill:$color;fill-opacity:1.0$stroke\"/>\n";
		++$total_blocks;
	}
	my $w = ($events{$name}[-1]{end} - $events{$name}[-1]{start}) * $scale;
	my $x = $margin + ($events{$name}[-1]{start} - $first) * $scale;
	print "<text transform=\"translate(".($x+$w+$text_offset).",".($y+$h/2).")\">$name</text>\n";
	$y += $h + $interline;
}

{
	$y += 2 * ($h + $interline);
	my $x = $margin;
	my $w = $stride * $scale;
	# asynchronous
	print "<rect x=\"$x\" width=\"$w\" y=\"$y\" height=\"$h\" style=\"fill:#d9d4cd;fill-opacity:1.0;stroke-width:0.2;stroke:#9ece6a\"/>\n";
	print "<text transform=\"translate(".($x+$w+$text_offset).",".($y+$h/2).")\">successful, asynchronous</text>\n";
	$y += $h + $interline;
	print "<rect x=\"$x\" width=\"$w\" y=\"$y\" height=\"$h\" style=\"fill:#d9d4cd;fill-opacity:1.0;stroke-width:0.2;stroke:#9d7cd8\"/>\n";
	print "<text transform=\"translate(".($x+$w+$text_offset).",".($y+$h/2).")\">deferred, asynchronous</text>\n";
	$y += $h + $interline;
	print "<rect x=\"$x\" width=\"$w\" y=\"$y\" height=\"$h\" style=\"fill:#d9d4cd;fill-opacity:1.0;stroke-width:0.2;stroke:#db4b4b\"/>\n";
	print "<text transform=\"translate(".($x+$w+$text_offset).",".($y+$h/2).")\">failed, asynchronous</text>\n";
	# synchronous
	$y += $h + $interline;
	print "<rect x=\"$x\" width=\"$w\" y=\"$y\" height=\"$h\" style=\"fill:#2ac3de;fill-opacity:1.0;stroke-width:0.2;stroke:#9ece6a\"/>\n";
	print "<text transform=\"translate(".($x+$w+$text_offset).",".($y+$h/2).")\">successful, synchronous</text>\n";
	$y += $h + $interline;
	print "<rect x=\"$x\" width=\"$w\" y=\"$y\" height=\"$h\" style=\"fill:#2ac3de;fill-opacity:1.0;stroke-width:0.2;stroke:#9d7cd8\"/>\n";
	print "<text transform=\"translate(".($x+$w+$text_offset).",".($y+$h/2).")\">deferred, synchronous</text>\n";
	$y += $h + $interline;
	print "<rect x=\"$x\" width=\"$w\" y=\"$y\" height=\"$h\" style=\"fill:#2ac3de;fill-opacity:1.0;stroke-width:0.2;stroke:#db4b4b\"/>\n";
	print "<text transform=\"translate(".($x+$w+$text_offset).",".($y+$h/2).")\">failed, synchronous</text>\n";
}

print "</svg>\n";
