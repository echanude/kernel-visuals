#!/usr/bin/env perl
# SPDX-License-Identifier: GPL-2.0-only

#@levels = (
#	{
#		"name" => <name>,
#		"start" => {
#			<func> => <time>,
#		},
#		"end" => {
#			<func> => <time>,
#		},
#	},[...]
#);

my @levels;
my $level = -1;

my $first = 999999;
my $last = 0;

sub round($$)
{
	my ($value, $precision) = @_;
	my $factor = 10 ** $precision;
	return int($value * $factor + 0.5) / $factor;
}

while (<>) {
	my $line = $_;
	# Skip comments.
	if ($line =~ /^#.*/) {
		next;
	}
	#                 TASK-PID    CPU#           ||||||    TIMESTAMP  initcall_level: level=LEVEL
	if ($line =~ /^\s+([^\s]+)\s+\[([0-9])+\]\s+([^\s]+)\s+([0-9.]+):\s+initcall_level: level=(.+)/) {
		my $task = $1;
		my $cpu = $2;
		my $flags = $3;
		my $ts = $4;
		my $name = $5;

		my $rec = { name => $name , start => {}, end => {} };
		push @levels, $rec;
		++$level;
		next;
	}
	#                 TASK-PID    CPU#           ||||||    TIMESTAMP  initcall_start: func=FUNCTION|SYMBOL [MODULE]
	if ($line =~ /^\s+([^\s]+)\s+\[([0-9])+\]\s+([^\s]+)\s+([0-9.]+):\s+initcall_start: func=(.+)/) {
		my $task = $1;
		my $cpu = $2;
		my $flags = $3;
		my $ts = $4;
		my $func = $5;

		$levels[$level]{start}->{$func} = $ts;

		if ($ts < $first) { $first = $ts }

		next;
	}
	#                 TASK-PID    CPU#           ||||||    TIMESTAMP  initcall_finish: func=FUNCTION|SYMBOL [MODULE] ret=RVAL
	if ($line =~ /^\s+([^\s]+)\s+\[([0-9])+\]\s+([^\s]+)\s+([0-9.]+):\s+initcall_finish: func=(.+) ret=([0-9-]+)/) {
		my $task = $1;
		my $cpu = $2;
		my $flags = $3;
		my $ts = $4;
		my $func = $5;
		my $ret = $6;

		$levels[$level]{end}->{$func} = $ts;

		if ($ts > $last) { $last = $ts; }
		next;
	}
	print STDERR "$line";
}

my $total_width = 8200;
my $margin = 50;
print "<?xml version=\"1.0\" standalone=\"no\"?>
<svg id=\"chart\" width=\"$total_width\" height=\"100%\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">
<style>
  text { fill:#fc7b7b; font-size:2.5pt }
  line { stroke:#e0af68; }
</style>

<rect width=\"100%\" height=\"100%\" fill=\"#16161e\"/>
";

my @styles;
$styles[0] = "fill:#2ac3de;fill-opacity:1.0;stroke-width:0";
$styles[1] = "fill:#d9d4cd;fill-opacity:1.0;stroke-width:0";
$styles[2] = "fill:#bb9af7;fill-opacity:1.0;stroke-width:0";

print STDERR scalar @levels;
print STDERR "\n";

my $y = 30;		# Starting offset.
my $h = 30;		# height of the bar.
my $interline = 50; 	# Space between timelines (used by text).
my $text_offset = -3;	# label vertical offset above each bar.
my $unit_offset = 3;	# units vertical offset under each graduation.
my $atom = 1; 		# Dipslay items at least 5 units large.

my $scale = ($total_width - (2 * $margin)) / ($last - $first); # scale timespan over our unit scale.
my $stride = round(($last - $first) / 200, 3); # Units granularity on the plot.

print STDERR "[ $first ; $last ], stride: $stride, scale: $scale\n";

# Time scale.
my $time = round($first, 3);
while ($time < ($last + $stride / 2)) {
	my $x1 = round($margin + ($time - $first) * $scale, 3);
	my $y1 = scalar(@levels) * ($interline + $h) + $h + $unit_offset;
	my $y_unit = scalar(@levels) * ($interline + $h) + $h + $unit_offset;
	my $time_text = sprintf("%.3f", $time);

	print "<text transform=\"translate($x1,$unit_offset)\" text-anchor=\"middle\">$time_text</text>\n";
	print "<line x1=\"$x1\" y1=\"".($unit_offset)."\" x2=\"$x1\" y2=\"$y1\" stroke-dasharray=\"1,8\" style=\"stroke:#e0af68;stroke-width:0.2\"/>\n";
	print "<text transform=\"translate($x1,$y_unit)\" text-anchor=\"middle\">$time_text</text>\n";
	$time = $time + $stride;
}

# Time blocks
foreach my $lvl (@levels) {
	my @starts = sort { $lvl->{start}{$a} <=> $lvl->{start}{$b} } keys %{$lvl->{start}};
	my @ends = sort { $lvl->{end}{$a} <=> $lvl->{end}{$b} } keys %{$lvl->{end}};

	my $noise = 0;

	print STDERR "$lvl->{name}: [ $first ; $last ], scale: $scale.\n";

	# Display time blocks for this initcall level.
	my $style = 0;
	foreach my $key (@starts) {
		my $dt = $lvl->{end}{$key} - $lvl->{start}{$key};
		my $x = $margin + ($lvl->{start}{$key} - $first) * $scale;
		my $w = $dt * $scale;

		if ($w >= $atom) {
		       print "<rect x=\"$x\" width=\"$w\" y=\"$y\" height=\"$h\" style=\"$styles[$style]\"/>\n";
		       print "<text transform=\"translate($x,".($y+$text_offset).") rotate(-30)\">$key</text>\n";
		       $style = ($style + 1) % scalar(@styles);
		} else {
			++$noise;
		}
	}

	# Debug: print the percentage of values that didn't make the cut.
	$noise = $noise / scalar(@starts) * 100;
	print STDERR "noise: $noise%\n";

	# Annotate each sequence by level name in the margin.
	print "<text transform=\"translate(5,".($y+$h/2).")\" style=\"font-size:6pt\">$lvl->{name}</text>\n";

	$y = $y + $h + $interline;
}

print "</svg>\n";
